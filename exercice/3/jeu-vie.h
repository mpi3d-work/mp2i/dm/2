#ifndef JEU_VIE_H
#define JEU_VIE_H

// 1.
bool **init_jeu(int ligne, int colone);

void free_jeu(bool **jeu, int ligne, int colone);

// 2.
void affiche_jeu(bool **jeu, int ligne, int colone);

// 3.
int nb_voisines_vivantes(bool **jeu, int ligne, int colone, int x, int y);

// 4.
void tour_jeu(bool **jeu, int ligne, int colone);

#endif
