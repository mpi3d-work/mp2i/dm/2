#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// 1.
bool **init_jeu(int ligne, int colone)
{
    bool **jeu = malloc(ligne * sizeof(bool *));
    assert(jeu != NULL);

    for (int i = 0; i < ligne; i += 1)
    {
        bool *ln = malloc(colone * sizeof(bool));
        assert(ln != NULL);

        for (int j = 0; j < colone; j += 1)
            ln[j] = rand() % 2 == 0;

        jeu[i] = ln;
    }

    return jeu;
}

void free_jeu(bool **jeu, int ligne, int colone)
{
    (void)colone; // pour eviter: unused parameter 'colone' [-Werror=unused-parameter]

    for (int i = 0; i < ligne; i += 1)
        free(jeu[i]);

    free(jeu);
}

// 2.
void affiche_jeu(bool **jeu, int ligne, int colone)
{
    int c = colone + 1; // charactère de saut de ligne ('\n')
    int l = c * ligne;
    char *a = malloc((l +
                      1 // sentinelle ('\0')
                      ) *
                     sizeof(char));
    assert(a != NULL);

    for (int i = 0; i < ligne; i += 1)
    {
        for (int j = 0; j < colone; j += 1)
        {
            char f;
            if (jeu[i][j])
                f = 'X';
            else
                f = '.';
            a[i * c + j] = f;
        }
        a[i * c + colone] = '\n';
    }
    a[l] = '\0';

    // on à construit une chaine de caractères pour éviter d'appeler printf en boucle
    printf("%s", a);

    free(a);
}

// 3.
int nb_voisines_vivantes(bool **jeu, int ligne, int colone, int x, int y)
{
    int s = 0;

    int haut = x - 1;
    int bas = x + 1;
    int gauche = y - 1;
    int droite = y + 1;

    if (haut >= 0)
    {
        if (jeu[haut][y])
            s += 1;

        if (gauche >= 0)
            if (jeu[haut][gauche])
                s += 1;

        if (droite < colone)
            if (jeu[haut][droite])
                s += 1;
    }

    if (bas < ligne)
    {
        if (jeu[bas][y])
            s += 1;

        if (gauche >= 0)
            if (jeu[bas][gauche])
                s += 1;

        if (droite < colone)
            if (jeu[bas][droite])
                s += 1;
    }

    if (gauche >= 0)
        if (jeu[x][gauche])
            s += 1;

    if (droite < colone)
        if (jeu[x][droite])
            s += 1;

    return s;
}

// 4.
bool nouvelle_valeur(bool **jeu, int ligne, int colone, int x, int y)
{
    int n = nb_voisines_vivantes(jeu, ligne, colone, x, y);
    if (n == 3 || (n == 2 && jeu[x][y]))
        return true;
    return false;
}

void tour_jeu(bool **jeu, int ligne, int colone)
{
    int m_ligne = ligne - 1;
    int m_colone = colone - 1;

    bool *tmps = malloc(sizeof(bool) * colone);
    assert(tmps != NULL);

    for (int j = 0; j < colone; j += 1)
        tmps[j] = nouvelle_valeur(jeu, ligne, colone, 0, j);

    for (int i = 1; i < ligne; i += 1)
    {
        bool tmp = tmps[0];
        tmps[0] = nouvelle_valeur(jeu, ligne, colone, i, 0);

        for (int j = 1; j < colone; j += 1)
        {
            bool val = nouvelle_valeur(jeu, ligne, colone, i, j);
            jeu[i - 1][j - 1] = tmp;
            tmp = tmps[j];
            tmps[j] = val;
        }

        jeu[i - 1][m_colone] = tmp;
    }

    free(jeu[m_ligne]);
    jeu[m_ligne] = tmps;
}

// 5.
bool streq(char *a, char *b)
{
    while (*a == *b)
    {
        if (*a == '\0')
            return true;
        a += 1;
        b += 1;
    }
    return false;
}

bool satisfait()
{
    char r[4];
    printf("satisfait ?\n");
    while (true)
    {
        scanf("%3s", r);
        if (streq(r, "oui"))
            return true;
        if (streq(r, "non"))
            return false;
        printf("pas compris, oui ou non ?\n");
    }
}

int main(int argc, char const *argv[])
{
    if (argc != 3)
    {
        printf("nombre invalide d'arguments\n");
        return 1;
    }
    int ligne = atoi(argv[1]);
    int colone = atoi(argv[2]);

    // 1.
    srand(time(NULL));

    bool **jeu;
    while (true)
    {
        jeu = init_jeu(ligne, colone);
        affiche_jeu(jeu, ligne, colone);
        if (satisfait())
            break;
        free_jeu(jeu, ligne, colone);
    }

    int tour;
    printf("nombre de tours ?\n");
    scanf("%d", &tour);

    // on affiche le tour 0
    system("clear");
    affiche_jeu(jeu, ligne, colone);
    fflush(stdout);

    for (int i = 0; i < tour; i += 1)
    {
        sleep(2);
        tour_jeu(jeu, ligne, colone);
        system("clear");
        affiche_jeu(jeu, ligne, colone);
        fflush(stdout);
    }

    free_jeu(jeu, ligne, colone);
    return 0;
}
