(* 1. *)
let rec indice_maximum = function
  | [] -> failwith "pas d'indice maximum"
  | [ _ ] -> 0
  | _ :: l -> indice_maximum l + 1
(* indice_maximum: O(n) *)

(* 2. *)
(* notons H(n): "indice_maximum d'une liste de taille n se termine
                 et renvoie l'indice maximum de la liste" avec n dans N*,
   indice_maximum [x] termine trivialement et renvoie 0 ce qui est indice maximum de [x]
   donc H(1)
   soit n dans N*, supposons H(n) vraie,
   alors indice_maximum [x(1); ...; x(n+1)] termine car indice_maximum [x(1); ...; x(n)] termine selon l'H.R.
   de plus la fonction renvoie la valeur sur un liste de taille n
   (supposé être la bonne valeur) et ajoute 1 donc on obtient la valeur souhaité au rang n + 1
   ainsi la fonction est totalement correcte car elle termine et renvoie la bonne valeur *)

(* 2. *)
let rec take l n =
  if n = 0 then []
  else
    match l with
    | [] -> failwith "liste trop petite"
    | x :: l -> x :: take l (n - 1)
(* take: O(n) *)

let rec drop l n =
  if n = 0 then l
  else
    match l with [] -> failwith "liste trop petite" | _ :: l -> drop l (n - 1)
(* drop: O(n) *)

(* 3. *)
(* Comparaison en entier. On ne peut pas utiliser a - b car pas polymorphe *)
let cmp a b =
  match (a, b) with a, b when a < b -> 1 | a, b when a > b -> -1 | _, _ -> 0
(* cmp: O(1) *)

let rec signe = function
  | [] | [ _ ] -> Some 0
  | a :: b :: l -> (
      match signe (b :: l) with
      | None -> None
      | Some s ->
          let c = cmp a b in
          if s * c < 0 then None else Some (s + c))
(* signe: O(n)
        * O(1) (cmp)
        = O(n) *)

let monotone l =
  match signe l with
  | Some s when s > 0 (* croissante *) -> 1
  | Some s when s < 0 (* decroissante *) -> 0
  | Some _ (* constante *) ->
      (* on ne sait pas quoi renvoyer si la liste est constante *)
      -1
  | None -> (* pas monotone *) -1
(* monotone: O(n) (signe)
           = O(n) *)

(* 4. *)
(* 6. *)
let rec constante = function
  | [] | [ _ ] -> true
  | a :: b :: l -> a = b && constante (b :: l)
(* constante: O(n) *)

let unimodale_v1 l =
  let rec _unimodale n =
    if n < 0 then false
    else
      ((let a = take l n in
        monotone a = 1 || constante a)
      &&
      let b = drop l n in
      monotone b = 0 || constante b)
      || _unimodale (n - 1)
  in
  _unimodale (indice_maximum l + 1)
(* unimodale_v1: O(n) (indice_maximum)
               + O(n) * (
               2 * (
                     O(n) (monotone)
                   + O(n) (constante)
                   )
                 + O(n) (take)
                 + O(n) (drop)
                        )
               = O(n²) *)

(* 5. *)
(* 6. *)
let rec decroissante = function
  | [] | [ _ ] -> true
  | a :: b :: l -> a >= b && decroissante (b :: l)
(* decroissante: O(n) *)

let rec unimodale_v2 = function
  | [] | [ _ ] -> true
  | a :: b :: l ->
      let l = b :: l in
      if a <= b then unimodale_v2 l else decroissante l
(* unimodale_v2: O(n) *)

(* 6. *)
(* unimodale_v2 plus performante que unimodale_v1 *)
