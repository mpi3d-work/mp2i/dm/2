#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

int longeur(char *str)
{
    char *s = str;
    while (*str != '\0')
        str += 1; // si le sujet dit vrai,
                  // on peut remplacer par 3 puisque toutes les chaines sont supposées être multiples de 3
                  // cela nous permettrait d'aller 3 * + vite

    return str - s;
}

// 9.

// 1.
// O(n) avec n la longeur de la chaine
bool est_sequence_adn(char *str)
{
    // variant: longeur(str)
    // invariant: la chaine de caractère jusqu'à str est une séquence d'ADN
    while (*str != '\0')
    {
        char c = *str;
        if (c != 'A' && c != 'C' && c != 'G' && c != 'T')
            return false;
        str += 1;
    }
    return true;
}

// 2.
// O(1)
char nucleotide_complementaire(char n)
{
    if (n == 'A')
        return 'T';
    if (n == 'C')
        return 'G';
    if (n == 'G')
        return 'C';
    if (n == 'T')
        return 'A';
    assert(false); // impossible
}

// 3.
// O(n) avec n la longeur de la chaine
char *deuxieme_brin(char *s)
{
    int l = longeur(s);
    char *b = malloc(sizeof(char) * (l + 1));
    assert(b != NULL);

    b[l] = '\0';
    l -= 1;
    // variant: l - i
    // invariant: b[0..i] est le deuxième brin de s[l-i..l]
    for (int i = 0; i <= l; i += 1)
        b[i] = nucleotide_complementaire(s[l - i]);

    return b;
}

// 4.
// O(n) avec n la longeur de la chaine b
bool est_double_brin(char *a, char *b)
{
    int l = longeur(b);
    int m = l - 1;
    // variant: l - i - 1
    // invariant: a[0..i] est le deuxième brin de b[m-i..m]
    for (int i = 0; i < l; i += 1)
    {
        char t = a[i];
        if (t == '\0' || nucleotide_complementaire(t) != b[m - i])
            return false;
    }
    return a[l] == '\0';
}

// 5.
// O(n) avec n la longeur de la chaine
char *adn_vers_arn(char *s)
{
    int l = longeur(s);
    char *a = malloc(sizeof(char) * (l + 1));
    assert(a != NULL);

    // variant: l - i - 1
    // invariant: a[0..i] est la chaine ARN correspondant à s[0..i]
    for (int i = 0; i < l; i += 1)
    {
        char c = s[i];
        if (c == 'T')
        {
            a[i] = 'U';
            continue;
        }
        a[i] = c;
    }
    a[l] = '\0';

    return a;
}

// 6.
// O(n) avec n la longeur de la chaine
char **codons(char *s)
{
    char *a = adn_vers_arn(s);
    int l = longeur(a);
    l /= 3;
    char **cs = malloc(sizeof(char *) * l);
    assert(cs != NULL);

    // variant: l - i - 1
    // invariant: cs[0..i] est la liste des codons de a[0..i*3]
    for (int i = 0; i < l; i += 1)
    {
        uint32_t *b = malloc(sizeof(uint32_t));
        assert(b != NULL);

        *b = *(uint32_t *)(a + i * 3);

        char *c = (char *)b;
        c[3] = '\0';
        cs[i] = c;
    }
    free(a);

    return cs;
}

// 7.
// O(n) avec n la longeur de la chaine
int nombre_occurrences_codon(char *adn, char *codon)
{
    int l = longeur(adn) / 3;
    char **cs = codons(adn);

    int n = 0;
    // variant: l - i - 1
    // invariant: n est le nombre d'occurence de codon dans cs[0..i]
    for (int i = 0; i < l; i += 1)
    {
        char *c = cs[i];
        if (codon[0] == c[0] && codon[1] == c[1] && codon[2] == c[2])
            n += 1;
        free(c);
    }

    free(cs);

    return n;
}

// la fonction avant de voir "devra appeler la fonction codons"
// O(n) avec n la longeur de la chaine
int nombre_occurrences_codon_v2(char *adn, char *codon)
{
    char a, b, c;
    if (codon[0] == 'U')
        a = 'T';
    else
        a = codon[0];
    if (codon[1] == 'U')
        b = 'T';
    else
        b = codon[1];
    if (codon[2] == 'U')
        c = 'T';
    else
        c = codon[2];

    int n = 0;
    // variant: longeur(adn)
    // invariant: n est le nombre d'occurence de codon dans jusqu'à adn
    while (*adn != '\0')
    {
        if (adn[0] == a && adn[1] == b && adn[2] == c)
            n += 1;
        adn += 3;
    }

    return n;
}

// 8.
int base_vers_index(char c)
{
    if (c == 'A')
        return 0;
    if (c == 'C')
        return 1;
    if (c == 'G')
        return 2;
    if (c == 'T' || c == 'U')
        return 3;
    assert(false); // impossible
}

// euh... petite fonction qui à chaque codon associe un index unique < 32.
// sachant que certain codon sont complémentaires, on s'arrange pour que les index des codons complémentaires soient les mêmes.
// pour faire simple: c'est un fonction de hashage sans collisions prenant le minimum de place mémoire.
const int table_de_hashage[4][4][4] = {
    {{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 7}, {11, 12, 13, 3}},
    {{14, 15, 16, 13}, {17, 18, 19, 10}, {20, 21, 19, 6}, {22, 23, 16, 2}},
    {{24, 25, 23, 12}, {26, 27, 21, 9}, {28, 27, 18, 5}, {29, 25, 15, 1}},
    {{30, 29, 22, 11}, {31, 28, 20, 8}, {31, 26, 17, 4}, {30, 24, 14, 0}}};

int codon_vers_index(char a, char b, char c)
{
    return table_de_hashage[base_vers_index(a)][base_vers_index(b)][base_vers_index(c)];
}

// O(n + m) avec n la longeur de la chaine adn et m la longeur de prot
bool proteine_dans_code_genetique(char *adn, char **prot, int prot_long)
{
    uint32_t codons = 0;
    // variant: prot_long - i - 1
    // invariant: codons est l'ensemble des codons de prot[0..i]
    for (int i = 0; i < prot_long; i += 1)
        codons |= 1 << codon_vers_index(prot[i][0], prot[i][1], prot[i][2]);

    // variant: longeur(adn)
    // invariant: codons est l'ensemble des codons restant à trouver dans adn
    while (*adn != '\0')
    {
        codons &= ~(1 << codon_vers_index(adn[0], adn[1], adn[2]));
        adn += 3;
    }

    return codons == 0;
}
