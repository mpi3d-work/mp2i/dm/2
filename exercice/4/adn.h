#ifndef ADN_H
#define ADN_H

// 1.
bool est_sequence_adn(char *str);

// 2.
char nucleotide_complementaire(char n);

// 3.
char *deuxieme_brin(char *s);

// 4.
bool est_double_brin(char *a, char *b);

// 5.
char *adn_vers_arn(char *s);

// 6.
char **codons(char *s);

// 7.
int nombre_occurrences_codon(char *adn, char *codon);

// 8.
bool proteine_dans_code_genetique(char *adn, char **prot, int prot_long);

#endif
