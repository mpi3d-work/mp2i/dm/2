#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

// 1.
void affiche_tab_chaines(char *tab[], int taille)
{
    for (int i = 0; i < taille; i++)
        printf("%s\n", tab[i]);
}

// 2.
int min(int x[], int taille)
{
    int m = x[0];
    for (int i = 1; i < taille; i += 1)
        if (x[i] < m)
            m = x[i];
    return m;
}

char boule(int n)
{
    if (n == 0)
        return '*';
    if (n < 27)
        return 'a' + n - 1;
    return ' ';
}

char **sapin_noel(int n)
{
    char **tab = malloc((n + 2) * sizeof(char *));
    assert(tab != NULL);

    for (int i = 0; i < n; i++)
    {
        char *ligne = malloc((n + i + 1) * sizeof(char));
        assert(ligne != NULL);

        for (int j = 0; j < n - i; j += 1)
            ligne[j] = ' ';
        for (int j = n - i; j < n + i; j += 2)
            ligne[j] = ' ';

        for (int j = n - i - 1; j < n + i; j += 2)
        {
            int d[] = {i, (n - i - 1) * 2, j - n + i + 1, n + i - j};
            ligne[j] = boule(min(d, 4) / 2);
        }

        ligne[n + i] = '\0';
        tab[i] = ligne;
    }

    char *ligne = malloc((n + 2) * sizeof(char));
    assert(ligne != NULL);

    for (int i = 0; i < n - 1; i++)
        ligne[i] = ' ';

    ligne[n - 2] = '|';
    ligne[n - 1] = '|';
    ligne[n] = '|';
    ligne[n + 1] = '\0';

    tab[n] = ligne;
    tab[n + 1] = ligne; // attention, lors de la deallocation, il ne faut pas liberer deux fois la meme ligne

    return tab;
}

// 3.
// Il y a bien un truc que je deteste faire en info, c'est du ASCII art.
