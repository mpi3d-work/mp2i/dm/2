(* 1.a. *)

exception Found of int

(* t doit être trié dans l'ordre croissant *)
let cherche t e =
  let a, b = (ref 0, ref (Array.length t)) in
  (* l'index i de la valeur cherchée e est dans l'intervalle [a; b[ *)
  try
    while !a < !b do
      (* (a ≠ b) *)
      let m = (!a + !b) / 2 (* milieu de l'intervalle [a; b[ *) in
      match t.(m) - e (* comparaison *) with
      | d when d > 0 (* t(m) > e *) -> b := m (* t(m) ≠ e donc i dans [a; m[ *)
      | d when d < 0 (* t(m) < e *) ->
          a := m + 1 (* t(m) ≠ e donc i dans ]m; b[ donc dans [m + 1; b[ *)
      | _ (* t(m) = e *) -> raise (Found m)
    done;
    None
    (* i dans [a; b[ mais a = b donc i dans Ø donc la valeur n'est pas dans le tableau *)
  with Found i -> Some i

(* 1.b. *)
(* variant v = b - a - 1
   en supposant que la longeur de t > 0, alors v ≥ 0 avant la boucle
   si v < 0 alors a ≥ b donc la boucle s'arrète
   à chaque iteration v' = (a - b) / 2 < v car a < b donc v strictement décroissant
   donc v est bien un variant donc la boucle s'arrête
*)
(* on suppose que la valeur e est dans le tableau
   invariant v = l'index i de la valeur cherchée e est dans l'intervalle [a; b[
   v est vraie avant la boucle car e supposée dans le tableau entier
   si i dans [a; b[, notons m le mileu de l'intervalle,
   comme le tableau est trié dans l'ordre croissant,
   si t(m) > e alors i < m, si t(m) < e, alors i > m
   sinon i = m donc on a fini
   donc on peut changer les bornes de l'intervalle en fonction de m
   tout en gardant i dans [a; b[
   ainsi l'invariant est toujours vrai
   si e n'est pas dans le tableau, alors on a toujours t(m) ≠ e,
   or la boucle s'arrète donc dans tout les cas la fonction est totalement correcte *)

(* toutes les instructions dans la boucle sont dans O(1),
   à chaque iteration, on diminue la taille du segment par 2
   donc la complexité de la boucle est dans O(1) * O(log(n)) = O(log(n))
   avec n la taille du tableau de depart,
   la complexité de la fonction est donc dans O(log(n)) *)

(* 2.a. *)
(* 2.c. *)

exception Break

(* m doit être une matrice carré *)
let constante_magique m =
  let l = Array.length m - 1 in
  (* on calcule les constantes des diagonales *)
  let a, b = (ref 0, ref 0) in
  (* variant = l - i *)
  (* invariant = a et b sont la somme respective des i + 1 premier element des deux diagonale *)
  for i = 0 to l do
    let t = m.(i) in
    a := !a + t.(i);
    (* diagonale (\) *)
    b := !b + t.(l - i)
    (* diagonale (/) *)
  done;
  if !a = !b then
    try
      (* on vérifie les lignes et les colones *)
      (* variant = l - i *)
      (* invariant = les i + 1 premières lignes et colones on une constante égale à a *)
      for i = 0 to l do
        let x, y = (ref 0, ref 0) in
        (* variant = l - j *)
        (* invariant = x et y sont la somme respective des i + 1 premier element de la i + 1 ligne et colone *)
        for j = 0 to l do
          x := !x + m.(i).(j);
          (* ligne (-) *)
          y := !y + m.(j).(i) (* colone (|) *)
        done;
        if !x <> !a || !y <> !a then raise Break (* constantes différentes *)
      done;
      Some !a
    with Break -> None
  else None

(* 2.b. *)
(* 2.c. *)

(* m doit être une matrice carré *)
let est_normal m =
  let l = Array.length m in
  let c = l * l in
  let l = l - 1 in
  let b = Array.make c false in
  (* afin de diminuer la complexité temporelle, on augmente la complexité spaciale a O(n²) *)
  (* on utilise ici la même idée que le principe des tiroirs, on prevois ici un tiroir par element
     (de 1 à n²) et on range les élément un par un dans son tiroir correspondant,
     et on s'arrète à la première impossibilité *)
  try
    (* variant = l - i *)
    (* invariant = la ligne i + 1 est valide *)
    for i = 0 to l do
      (* variant = l - j *)
      (* invariant = l'element au coordonnées (i, j) du tableau est conforme *)
      for j = 0 to l do
        let v = m.(i).(j) - 1 in
        if v < 0 || v >= c then raise Break;
        (* impossible car on ne peut pas avoir d'élément > n² *)
        if b.(v) then raise Break;
        (* impossible car on ne peut pas avoir 2 fois le même élément *)
        b.(v) <- true
      done
    done;
    true
  with Break -> false

(* 2.d. *)
let cree_carre_magique n =
  if n mod 2 = 0 then failwith "n pair"
  else
    (* n impair *)
    let a, c, m = (Array.make_matrix n n 0, n * n, n / 2) in
    for i = 0 to c - 1 do
      let j = i / n in
      a.((c
        (* on ajoute n² car le modulo d'ocaml ne retourne pas toujours un reste ≥ 0 *)
        - i
         + (2 * j))
         mod n).((m
                (* on ajoute n / 2 car on commence au milieu de la ligne *)
                + i
                 - j)
                 mod n) <- i + 1
      (* le code fait appel ici à beaucoup d'arithmétique *)
    done;
    a

(* 2.e. *)
(* n = longeur d'un coté de la matrice *)
(* constante_magique: O(n) + O(n) * O(n) = O(n²) *)
(* est_normal: O(n²) (Array.make)
             + O(n) * O(n)
             = O(n²) *)
(* cree_carre_magique: O(n²) (Array.make_matrix)
                     + O(n²)
                     = O(n²) *)
